<?php

namespace CategoryBundle\Entity;

use CategoryBundle\Model\AbstractContent;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Class contact
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="CategoryBundle\Entity\Repository\CategoryRepository")
 * @Vich\Uploadable
 */

class Category extends AbstractContent{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="svg_url", type="string", nullable=true)
     *
     */
    private $svg;

    /**
     * @var string
     *
     * @ORM\Column(name="select_n", type="integer", nullable=true)
     */
    private $selectN;

    /**
     * @var string
     * @ORM\Column(name="main_image_category", type="string", nullable=true)
     */
    private $mainImage;
    
    /**
     * @var File
    * @Vich\UploadableField(mapping="main_images", fileNameProperty="mainImage")
    */
    private $mainImageFile;

    /**
     * @var string
     * @ORM\Column(name="url_link", type="string", nullable=true)
     *
     */
    protected $urlLink;

    /**
     * @var string
     * @ORM\Column(name="url_src", type="string", nullable=true)
     *
     */
    protected $urlSrc;

    
    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * @param $svg
     * @return CategoryContent
     */
    public function setSvg($svg)
    {
        $this->svg = $svg;
        return $this;
    }

    /**
     * @return string
     */
    public function getSvg()
    {
        return $this->svg;
    }

    /**
     * @param $selectN
     * @return CategoryContent
     */
    public function setSelectN($selectN)
    {
        $this->selectN = $selectN;
        return $this;
    }

    /**
     * @return string
     */
    public function getSelectN()
    {
        return $this->selectN;
    }

          /**
     * @param $mainImage
     */
    public function setMainImage($mainImage)
    {
        $this->mainImage = $mainImage;
        return $this;
    }

    /**
     * @return string
     */
    public function getMainImage()
    {
        return $this->mainImage;
    }

    /**
     * @param File $mainImageFile
     */
    public function setMainImageFile(File $mainImageFile = null)
    {
        $this->mainImageFile = $mainImageFile;

        if ($mainImageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return File
     */
    public function getMainImageFile()
    {
        return $this->mainImageFile;
    }

    /**
     * @param $urlLink
     * @return PageContent
     */
    public function setUrlLink($urlLink)
    {
        $this->urlLink = $urlLink;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrlLink()
    {
        return $this->urlLink;
    }

    /**
     * @param $urlSrc
     * @return PageContent
     */
    public function setUrlSrc($urlSrc)
    {
        $this->urlSrc = $urlSrc;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrlSrc()
    {
        return $this->urlSrc;
    }

}
