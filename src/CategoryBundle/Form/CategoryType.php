<?php

namespace CategoryBundle\Form;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class CategoryType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, array('label'=> 'Заголовок', 'required' => false))
            ->add('selectN', ChoiceType::class, array(
                'choices' => array(
                    '0' => 'Тип съёмки',
                    '1' => 'Направление',
                ),
                'label'=> 'Выбрать',
            ))
            ->add('content', CKEditorType::class, array(
                'config' => array(
                    'uiColor' => '#ffffff',
                ),
                'label'=> 'Описание',
            ))
            ->add('mainImageFile', VichFileType::class, [
                'required' => false,
                'allow_delete' => true, 
                'label'=> 'Картинка категории',
            ])  
            ->add('svg', null, array('label'=> 'Иконка', 'required' => false))   
            ->add('urlLink', null, array(
                'label'=> 'Ссылка к источнику',
                 'required' => false))    
            ->add('urlSrc', null, array(
                'label'=> 'Ссылка ютуб преза',
                'required' => false)) 
            ->add('published', null, array('label'=>'Опубликованно?'))

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CategoryBundle\Entity\Category'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'contentbundle_category';
    }
}
