<?php

namespace CategoryBundle\Model;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

use Symfony\Component\HttpFoundation\File\File;


/**
 *  @ORM\MappedSuperclass
 *
 */
class AbstractContent 
{

    /**
     * @var string
     * @ORM\Column(name="title", type="string")
     *
     */
    protected $title;

    /**
     * @var string
     * @ORM\Column(name="slug", type="string", length=255, unique=true)
     * @Gedmo\Slug(fields={"title"})
     */
    protected $slug;

    /**
     * @var string
     * @ORM\Column(name="content", type="text")
     *
     */
    protected $content;


    /**
     * @var boolean
     * @ORM\Column(name="is_published", type="boolean")
     */
    protected $published;


    public function __construct()
    {
        $this->published = true;
    }
    /**
     * @param $title
     * @return AbstractContent
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param $content
     * @return AbstractContent
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param boolean $published
     * @return AbstractContent
     */
    public function setPublished($published)
    {
        $this->published = $published;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPublished()
    {
        return $this->published;
    }

        /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return AbstractCategory
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return ($this->title)?$this->title:'-';
    }


}