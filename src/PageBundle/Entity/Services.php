<?php

namespace PageBundle\Entity;

use PageBundle\Model\AbstractContent;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class Services
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="PageBundle\Entity\Repository\ServicesRepository")
 * @Vich\Uploadable
 */

class Services extends AbstractContent{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     * @Gedmo\Slug(fields={"title"}, updatable=false)
     */
     private $link;

    
    /**
     * @var string
     * @ORM\Column(name="main_image", type="string", nullable=true)
     */
     private $mainImage;

     /**
      * @var File
      * @Vich\UploadableField(mapping="main_images", fileNameProperty="mainImage")
      */
     private $mainImageFile;



        
    /**
     * @var string
     * @ORM\Column(name="main_image_slider", type="string", nullable=true)
     */
    private $mainSliderImage;
    
    /**
     * @var File
    * @Vich\UploadableField(mapping="main_images", fileNameProperty="mainSliderImage")
    */
    private $mainSliderImageFile;
    
    /**
     * @var string
     * @ORM\Column(name="main_image_desc", type="string", nullable=true)
     */
    private $mainImageDescription;

    /**
     * @var File
    * @Vich\UploadableField(mapping="main_images", fileNameProperty="mainImageDescription")
    */
    private $mainImageDescriptionFile;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Services
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return Services
     */
     public function setLink($link)
     {
         $this->link = $link;
 
         return $this;
     }
 
     /**
      * Get link
      *
      * @return string 
      */
     public function getLink()
     {
         return $this->link;
     }


    /**
     * Set published
     *
     * @param boolean $published
     * @return Services
     */
     public function setPublished($published)
     {
         $this->published = $published;
 
         return $this;
     }
 
     /**
      * Get published
      *
      * @return boolean 
      */
     public function getPublished()
     {
         return $this->published;
     }


      /**
     * @param $mainImage
     */
    public function setMainImage($mainImage)
    {
        $this->mainImage = $mainImage;
        return $this;
    }

    /**
     * @return string
     */
    public function getMainImage()
    {
        return $this->mainImage;
    }

    /**
     * @param File $mainImageFile
     */
    public function setMainImageFile(File $mainImageFile = null)
    {
        $this->mainImageFile = $mainImageFile;

        if ($mainImageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return File
     */
    public function getMainImageFile()
    {
        return $this->mainImageFile;
    }


    /**
     * @param $mainSliderImage
     */
    public function setMainSliderImage($mainSliderImage)
    {
        $this->mainSliderImage = $mainSliderImage;
        return $this;
    }

    /**
     * @return string
     */
    public function getMainSliderImage()
    {
        return $this->mainSliderImage;
    }

    /**
     * @param File $mainSliderImageFile
     */
    public function setMainSliderImageFile(File $mainSliderImageFile = null)
    {
        $this->mainSliderImageFile = $mainSliderImageFile;

        if ($mainSliderImageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return File
     */
    public function getMainSliderImageFile()
    {
        return $this->mainSliderImageFile;
    }

          /**
     * @param $mainImageDescription
     */
    public function setMainImageDescription($mainImageDescription)
    {
        $this->mainImageDescription = $mainImageDescription;
        return $this;
    }

    /**
     * @return string
     */
    public function getMainImageDescription()
    {
        return $this->mainImageDescription;
    }

    /**
     * @param File $mainImageDescriptionFile
     */
    public function setMainImageDescriptionFile(File $mainImageDescriptionFile = null)
    {
        $this->mainImageDescriptionFile = $mainImageDescriptionFile;

        if ($mainImageDescriptionFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return File
     */
    public function getMainImageDescriptionFile()
    {
        return $this->mainImageDescriptionFile;
    }

}
