<?php

namespace PageBundle\Entity;

use PageBundle\Model\AbstractContent;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class Page
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="PageBundle\Entity\Repository\PageRepository")
 * @Vich\Uploadable
 */

class Page extends AbstractContent{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     * @Gedmo\Slug(fields={"title"}, updatable=false)
     */
     private $link;

    
    /**
     * @var string
     * @ORM\Column(name="main_image", type="string", nullable=true)
     */
     private $mainImage;

     /**
      * @var File
      * @Vich\UploadableField(mapping="main_images", fileNameProperty="mainImage")
      */
     private $mainImageFile;


     /**
     * 
     * @ORM\ManyToOne(targetEntity="CategoryBundle\Entity\Category", inversedBy="pages")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category; 


    /**
     * @var string
     * @ORM\Column(name="content_two", type="text")
     *
     */
    private $contentTwo;

    /**
     * @var string
     * @ORM\Column(name="url_video", type="string", nullable=true)
     *
     */
    protected $urlVideo;

    /**
     * @var string
     * @ORM\Column(name="url_video_two", type="string", nullable=true)
     *
     */
    protected $urlVideoTwo;

        
    /**
     * @var string
     * @ORM\Column(name="main_image_slider", type="string", nullable=true)
     */
    private $mainSliderImage;
    
    /**
     * @var File
    * @Vich\UploadableField(mapping="main_images", fileNameProperty="mainSliderImage")
    */
    private $mainSliderImageFile;

    public function __construct()
    {
        $this->category = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Page
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return Page
     */
     public function setLink($link)
     {
         $this->link = $link;
 
         return $this;
     }
 
     /**
      * Get link
      *
      * @return string 
      */
     public function getLink()
     {
         return $this->link;
     }


    /**
     * Set published
     *
     * @param boolean $published
     * @return Page
     */
     public function setPublished($published)
     {
         $this->published = $published;
 
         return $this;
     }
 
     /**
      * Get published
      *
      * @return boolean 
      */
     public function getPublished()
     {
         return $this->published;
     }


      /**
     * @param $mainImage
     */
    public function setMainImage($mainImage)
    {
        $this->mainImage = $mainImage;
        return $this;
    }

    /**
     * @return string
     */
    public function getMainImage()
    {
        return $this->mainImage;
    }

    /**
     * @param File $mainImageFile
     */
    public function setMainImageFile(File $mainImageFile = null)
    {
        $this->mainImageFile = $mainImageFile;

        if ($mainImageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return File
     */
    public function getMainImageFile()
    {
        return $this->mainImageFile;
    }

     /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     * @return Category
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }
     /**
     * @param $contentTwo
     * @return PageContent
     */
    public function setContentTwo($contentTwo)
    {
        $this->contentTwo = $contentTwo;
        return $this;
    }

    /**
     * @return string
     */
    public function getContentTwo()
    {
        return $this->contentTwo;
    }

    /**
     * @param $urlVideo
     * @return PageContent
     */
    public function setUrlVideo($urlVideo)
    {
        $this->urlVideo = $urlVideo;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrlVideo()
    {
        return $this->urlVideo;
    }
    /**
     * @param $urlVideoTwo
     * @return PageContent
     */
    public function setUrlVideoTwo($urlVideoTwo)
    {
        $this->urlVideoTwo = $urlVideoTwo;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrlVideoTwo()
    {
        return $this->urlVideoTwo;
    }


      /**
     * @param $mainSliderImage
     */
    public function setMainSliderImage($mainSliderImage)
    {
        $this->mainSliderImage = $mainSliderImage;
        return $this;
    }

    /**
     * @return string
     */
    public function getMainSliderImage()
    {
        return $this->mainSliderImage;
    }

    /**
     * @param File $mainSliderImageFile
     */
    public function setMainSliderImageFile(File $mainSliderImageFile = null)
    {
        $this->mainSliderImageFile = $mainSliderImageFile;

        if ($mainSliderImageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return File
     */
    public function getMainSliderImageFile()
    {
        return $this->mainSliderImageFile;
    }

    

}
