<?php

namespace PageBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;

class ServicesController extends Controller
{

    /**
     * @Route("/services/{link}", name="services_show", requirements={"link"=".+"})
     *
    */
    public function showServicesAction($link)
    {

        $em = $this->getDoctrine()->getManager();
        $page = $em->getRepository('PageBundle:Services')->findOneBy(array('link' => $link));

        if (!$page) {
            throw $this->createNotFoundException('Unable to find Page entity.');
        }

        return $this->render('Services/show.html.twig', array(
            'page' => $page,
        ));
    }

}