<?php

namespace PageBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
* @package PageBundle\Controller@
* @Route("/about", name="about_index")
*/
class AboutController extends Controller
{
    /**
     * @package PageBundle\Controller@
     * @Route("/", name="about_show")
     */
    public function indexAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('PageBundle:About')->findBy(array('published' => true));
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Page entity.');
        }
        return $this->render('About/index.html.twig', array(
            'entity' => $entity,
        ));
    }

}