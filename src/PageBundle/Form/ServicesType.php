<?php

namespace PageBundle\Form;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use CategoryBundle\Entity\Category;
use Doctrine\Common\Collections\ArrayCollection;

class ServicesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, array('label'=> 'Заголовок', 'required' => false))
            ->add('link', null, array('label'=> 'Ссылка', 'required' => false))
            ->add('content', CKEditorType::class, array(
                'config' => array(
                    'uiColor' => '#ffffff',
                ),
                'label'=> 'Описание задачи',
            ))
            ->add('mainImageFile', VichFileType::class, [
                'required' => false,
                'allow_delete' => false, 
                'label'=> 'Картинка (в заголовке)',
            ])         
            
            ->add('mainImageDescriptionFile', VichFileType::class, [
                'required' => false,
                'allow_delete' => false, 
                'label'=> 'Картинка (в описании)',
            ])   

            ->add('mainSliderImageFile', VichFileType::class, [
                'required' => false,
                'allow_delete' => false, 
                'label'=> 'Картинка (внизу)',
            ])

            ->add('metaTitle', null, array('label'=> 'SEO - Заголовок', 'required' => false))
            ->add('metaDescription', null, array('label'=> 'SEO - Описание', 'required' => false))
            ->add('metaKeywords', null, array('label'=> 'SEO - Ключевые слова', 'required' => false))

            ->add('published', null, array('label'=>'Опубликованно?'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PageBundle\Entity\Services'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'contentbundle_services';
    }
}
