<?php

namespace LanBundle\Form;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\Common\Collections\ArrayCollection;

class LanType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, array('label'=> 'Заголовок', 'required' => false))
            ->add('link', null, array('label'=> 'Ссылка на соц сеть', 'required' => false))
            ->add('mainImageFile', VichFileType::class, [
                'required' => false,
                'allow_delete' => false, 
                'label'=> 'Логотип соц сети',
            ])         
            ->add('published', null, array('label'=>'Опубликованно?'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'LanBundle\Entity\Lan'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'contentbundle_lan';
    }
}
