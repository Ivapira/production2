<?php

namespace LanBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;

class LanController extends Controller
{

    public function getLanMenuAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('LanBundle:Lan')->findBy(array('published' => true));
        return $this->render('Lan/show.html.twig', array(
            'entities' => $entities,
        ));
    }


}