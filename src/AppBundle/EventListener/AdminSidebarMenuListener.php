<?php

namespace AppBundle\EventListener;


// ...

use Avanzu\AdminThemeBundle\Event\SidebarMenuEvent;
use Avanzu\AdminThemeBundle\Model\MenuItemModel;
use Symfony\Component\HttpFoundation\Request;

class AdminSidebarMenuListener {

    // ...

    public function onSetupMenu(SidebarMenuEvent $event) {

        $request = $event->getRequest();

        foreach ($this->getMenu($request) as $item) {
            $event->addItem($item);
        }

    }

    protected function getMenu(Request $request) {
        // Build your menu here by constructing a MenuItemModel array
        $menuItems = array(
            $content = new MenuItemModel('page', 'Проекты', false, array(/* options */), 'iconclasses fa fa-file-text-o'),
            $teams = new MenuItemModel('page1', 'Команда', false, array(/* options */), 'iconclasses fa fa-file-text-o'),
            $services = new MenuItemModel('page2', 'Услуги', false, array(/* options */), 'iconclasses fa fa-file-text-o'),
            $contacts = new MenuItemModel('page3', 'Контакты', false, array(/* options */), 'iconclasses fa fa-file-text-o'),
            $soc = new MenuItemModel('page4', 'Соц сети', false, array(/* options */), 'iconclasses fa fa-file-text-o')
        );

        $content->addChild(new MenuItemModel('category', 'Категории', 'category_page', array()));
        $content->addChild(new MenuItemModel('pages', 'Страницы', 'admin_page', array()));

        $services->addChild(new MenuItemModel('services', 'Страницы', 'admin_services', array()));
        
        $teams->addChild(new MenuItemModel('about', 'О нас', 'about_page', array()));
        $teams->addChild(new MenuItemModel('teams', 'Homosapien', 'team_page', array()));
        $contacts->addChild(new MenuItemModel('contacts', 'Страница', 'contact_page', array()));

        $soc->addChild(new MenuItemModel('lan', 'Страницы', 'admin_lan', array()));
        return $this->activateByRoute($request->get('_route'), $menuItems);
    }

    protected function activateByRoute($route, $items) {

        foreach($items as $item) {
            if($item->hasChildren()) {
                $this->activateByRoute($route, $item->getChildren());
            }
            else {
                if($item->getRoute() == $route) {
                    $item->setIsActive(true);
                }
            }
        }

        return $items;
    }

}